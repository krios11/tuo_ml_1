from joblib import load
import numpy as np
import pyodbc 
import os


# set env variables
serverName =    'DESKTOP-O618OHR\SQLEXPRESS'
databaseName =  'datawarehouseTUOExample'
tableName =     'input_model_customer_360_levels_ds'
userId =        None 
userPassword =  None

# get trained models
dirname =               os.path.dirname(__file__)
configLevelModel =      load(f"{dirname}/trained_models/configLevelModel_2_100%.joblib")
sociabilityLevelModel = load(f"{dirname}/trained_models/sociabilityLevelModel_62%.joblib")
marketingLevelModel =   load(f"{dirname}/trained_models/marketingLevelModel_75%.joblib")
Scaler =                load(f'{dirname}/trained_models/scalers/first_ml_model_scaler.joblib')


# get data from database
conn = pyodbc.connect('Driver={SQL Server};'
                      f'Server={serverName};'
                      f'Database={databaseName};'
                      'Trusted_Connection=yes;'
                      f'UID={userId};'
                      f'PWD={userPassword};')
cursor = conn.cursor()


# get and cast data from database
cursor.execute(f'''
  SELECT 
    company_id,
    CAST(added_image AS INT)                AS added_image,
    CAST(added_ideal_client AS INT)         AS added_ideal_client,
    CAST(allow_to_use_company_image AS INT) AS allow_to_use_company_image,
    total_conversations,
    total_publications,
    total_business_valuations
  FROM {tableName}
''')
result = cursor.fetchall()


# set predictions for each customer
for i in result:

  #  set inputs
  row = np.array(i)
  X_original = np.array([row[1:7]])
  index = i[0]
  X = Scaler.transform(X_original)

  # get predictions and set as outputs
  Y_pred_config_level =      np.array(configLevelModel.predict(X))
  Y_pred_sociability_level = np.array(sociabilityLevelModel.predict(X))
  Y_pred_marketing_level =   np.array(marketingLevelModel.predict(X))

  print(f'Prediction results: {Y_pred_config_level[0]}, {Y_pred_sociability_level[0]}, {Y_pred_marketing_level[0]}')

  # update row in database
  cursor.execute(f'''
    UPDATE { tableName }
    SET profile_level_configuration = { Y_pred_config_level[0] },
        sociability_level           = { Y_pred_sociability_level[0] },
        marketing_level             = { Y_pred_marketing_level[0] }
    WHERE company_id = '{ index }'
  ''')

  # commit changes
  conn.commit()


# show finish message
print(f'Predictions were successfully saved in: {databaseName} -> {tableName}')


